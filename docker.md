# Docker


## what is container ?
   - Traditional VM's virtualize hardware, so operation system run on hardware abstraction layer on hypervisor.
   - Container virtualize the operationg system so the abstraction is done within the host operationg system to the guest container.

## advantages
  - portability across machines and infracture
  - version control
  - sharing
  - lightweight with minimal overhead

## dis-advantages
  - less isolated than a full vm because container all share the same kernal per given host.
  - portability to unlike operating system is a challenge linux containers cannot run on windows and vice-versa
  - containers aren't for everything.some apps won't run well in containers.

## mostly whn use :

  - software requires complex configuration, partucular versions of libraries, or complex dependencies.
  - software needs greater permissions than offered by the shared hosting but not the resources of a virtal or phical machine.
  - an appication scale is smaller than that physical or virtual machine.
  - easy applicaion portability between hosts is required.

## when not use:
  - when a simple,shared hosting will suffice.
  - to deploy network infrastructure
  - as fully replacements for virtual machines.
  - whn an applicatoins load requires a dedicated or multiple dedicated machines to run.
  - to run multiple applications in a single containers

## what is docker ?
Docker containers wrap up a piece of softwre in a complete filesystem that contains everything  it needs to run:
code,runtime,system tools,system libraries, anything you can install on a server. this gurantees that it will alwalys run the same, regardless of the env it is running in.

## development vs production

  - while docker is natively a linux based technology, docker development and testing can be done in windows, osx, or linux.
  - docker provides software bundles that pre-built linux vm running on virtualbox for building and testing docker containers called docker toolbox.
  - before u begin:
  - make sure you are running a 64-bit version of the OS.
  - ensure that hardware virtualizaion is turned on.
## docker hub:

  - docker hub is a repository of pre-built images.
  - an image is a template from which is containers are deployed.


## Downloading ("pulling") Docker Images
  - Docker has a command for downloading an image by name:
```
docker pull repository:tag
```
  - and also running an image will invoke a pull if  the images is not in the local repository.
```
docker run repository:tag
```
## creating containers
  - Docker Containers are created with "create" command or the "run" command which will create and start the container.
  - The Run command takes a number of paramaters
  - some parameters are docker parameters
## Common run/Create options


| options        | Description           | 
| ------------- |:-------------:| 
| --name      | Name of the container | 
| -p      | publish port and internal port in the xx::yy format.if the host is multi-homed,the user can bing an ip and port aaa.bbb.ccc.ddd.xx:yy     | 
| -d | run in detached mode(run only)   |   
| --link | link to another container.this is useful for commmunication between containers     |   
| -e | set environment variable     |   

## Managing containers

| options       | Description           | 
| ------------- |:-------------:| 
|ps             | list running containers | 
| ps -a         | list all containers    | 
| start        | start a container by name or ID   |   
| stop | stop a containers    |   
| rm  | remove stoped containers    |   
| rm  -f| forcely remove containers    |
| attch| attach to a running container   |
| exec | run a command in a containers    |

## How to Build image

The Dockerfile is a text document that contains instructions that are interpeted by Docker build to create images
The Conversion for Dockerfile is 
```
INSTRUCTION arguments
```

the instruction in the dockerfile can build images existing images by importing files and executing commands to customize the image.

### Instructions

* [.dockerignore](https://docs.docker.com/engine/reference/builder/#dockerignore-file)
* [FROM](https://docs.docker.com/engine/reference/builder/#from) Sets the Base Image for subsequent instructions.
* [MAINTAINER (deprecated - use LABEL instead)](https://docs.docker.com/engine/reference/builder/#maintainer-deprecated) Set the Author field of the generated images.
* [RUN](https://docs.docker.com/engine/reference/builder/#run) execute any commands in a new layer on top of the current image and commit the results.
* [CMD](https://docs.docker.com/engine/reference/builder/#cmd) provide defaults for an executing container.
* [EXPOSE](https://docs.docker.com/engine/reference/builder/#expose) informs Docker that the container listens on the specified network ports at runtime.  NOTE: does not actually make ports accessible.
* [ENV](https://docs.docker.com/engine/reference/builder/#env) sets environment variable.
* [ADD](https://docs.docker.com/engine/reference/builder/#add) copies new files, directories or remote file to container.  Invalidates caches. Avoid `ADD` and use `COPY` instead.
* [COPY](https://docs.docker.com/engine/reference/builder/#copy) copies new files or directories to container.  Note that this only copies as root, so you have to chown manually regardless of your USER / WORKDIR setting.  See https://github.com/moby/moby/issues/30110
* [ENTRYPOINT](https://docs.docker.com/engine/reference/builder/#entrypoint) configures a container that will run as an executable.
* [VOLUME](https://docs.docker.com/engine/reference/builder/#volume) creates a mount point for externally mounted volumes or other containers.
* [USER](https://docs.docker.com/engine/reference/builder/#user) sets the user name for following RUN / CMD / ENTRYPOINT commands.
* [WORKDIR](https://docs.docker.com/engine/reference/builder/#workdir) sets the working directory.
* [ARG](https://docs.docker.com/engine/reference/builder/#arg) defines a build-time variable.
* [ONBUILD](https://docs.docker.com/engine/reference/builder/#onbuild) adds a trigger instruction when the image is used as the base for another build.
* [STOPSIGNAL](https://docs.docker.com/engine/reference/builder/#stopsignal) sets the system call signal that will be sent to the container to exit.
* [LABEL](https://docs.docker.com/engine/userguide/labels-custom-metadata/) apply key/value metadata to your images, containers, or daemons.



